# MIPS Asm
### About
MIPS Asm is a MIPS relocatable assembler which translates MIPS Assembly language programs to MIPS machine language programs. MIPS Asm is written entirely in C++08 and builds on most UNIX-like operating systems (i.e.: Linux or Mac).

### Screenshot
#### Sample Interaction
![MIPS-Asm_Screenshot](https://cloud.githubusercontent.com/assets/7763904/7487583/79864e2c-f388-11e4-9b44-96cf8936d6a8.png)

### Compilation
```Bash
make
```

### Clean Build
```Bash
make clean
```

### Usage
```Bash
./asm
```

### MIPS Instructions Supported
Instruction Name         | Assembly Code   | Machine Code                            | Operation
:------------------------|:----------------|:----------------------------------------|:-------------------------
Word                     | .word i         | iiii iiii iiii iiii iiii iiii iiii iiii |
Add                      | add $d, $s, $t  | 0000 00ss ssst tttt dddd d000 0010 0000 | $d = $s + $t
Subtract                 | sub $d, $s, $t  | 0000 00ss ssst tttt dddd d000 0010 0010 | $d = $s - $t
Multiply                 | mult $s, $t     | 0000 00ss ssst tttt 0000 0000 0001 1000 | hi:lo = $s * $t
Multiply Unsigned        | multu $s, $t    | 0000 00ss ssst tttt 0000 0000 0001 1001 | hi:lo = $s * $t
Divide                   | div $s, $t      | 0000 00ss ssst tttt 0000 0000 0001 1010 | lo = $s / $t; hi = $s % $t
Divide Unsigned          | divu $s, $t     | 0000 00ss ssst tttt 0000 0000 0001 1011 | lo = $s / $t; hi = $s % $t
Move From High/Remainder | mfhi $d         | 0000 0000 0000 0000 dddd d000 0001 0000 | $d = hi
Move From Low/Quotient   | mflo $d         | 0000 0000 0000 0000 dddd d000 0001 0010 | $d = lo
Load Immediate And Skip  | lis $d          | 0000 0000 0000 0000 dddd d000 0001 0100 | $d = MEM[pc]; pc = pc + 4
Load Word                | lw $t, i($s)    | 1000 11ss ssst tttt iiii iiii iiii iiii | $t = MEM[$s + i]
Store Word               | sw $t, i($s)    | 1010 11ss ssst tttt iiii iiii iiii iiii | MEM[$s + i] = $t
Set Less Than            | slt $d, $s, $t  | 0000 00ss ssst tttt dddd d000 0010 1010 | $d = 1 if $s < $t; 0 otherwise
Set Less Than Unsigned   | sltu $d, $s, $t | 0000 00ss ssst tttt dddd d000 0010 1011 | $d = 1 if $s < $t; 0 otherwise
Branch On Equal          | beq $s, $t, i   | 0001 00ss ssst tttt iiii iiii iiii iiii | if ($s == $t) pc += i * 4
Branch On Not Equal      | bne $s, $t, i   | 0001 01ss ssst tttt iiii iiii iiii iiii | if ($s != $t) pc += i * 4
Jump Register            | jr $s           | 0000 00ss sss0 0000 0000 0000 0000 1000 | pc = $s
Jump And Link Register   | jalr $s         | 0000 00ss sss0 0000 0000 0000 0000 1001 | temp = $s; $31 = pc; pc = temp

### License
MIPS Asm is licensed under the [MIT License](https://github.com/elailai94/MIPS-Asm/blob/master/LICENSE.md).
