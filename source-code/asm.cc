//==============================================================================
// MIPS Asm
//
// @description: Program for translating MIPS Assembly language programs to
//  MIPS machine language
// @author: ahlai
//==============================================================================

#include "kind.h"
#include "lexer.h"
#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <sstream>

// Use only the needed aspects of each namespace
using std::string;
using std::ostringstream;
using std::vector;
using std::map;
using std::endl;
using std::cerr;
using std::cin;
using std::cout;
using std::getline;
using ASM::Token;
using ASM::Lexer;

// output_word(word) prints out word to standard output
void output_word(int word) {
   char c = word >> 24;
   cout << c;
   c = word >> 16;
   cout << c;
   c = word >> 8;
   cout << c;
   c = word;
   cout << c;
} // output_word

// output_rtype(s, t, d, funct) outputs the format of a R-type instruction
//  to standard output
void output_rtype(int s, int t, int d, int funct) {
   int word = 0;
   s <<= 21;
   t <<= 16;
   d <<= 11;
   word = s | t | d | funct;
   output_word(word);
} // assemble_rtype

// output_itype(funct, s, t, i) outputs the format of an I-type instruction
//  to standard output
void output_itype(int funct, int s, int t, int i) {
   int word = 0;
   funct <<= 26;
   s <<= 21;
   t <<= 16;
   i &= 0xffff;
   word = funct | s | t | i;
   output_word(word);
} // assemble_itype

// output_jtype(s, funct) outputs the format of a J-type instruction
//  to standard output
void output_jtype(int s, int funct) {
   int word = 0;
   s <<= 21;
   word = s | funct;
   output_word(word);
} // assemble_jtype

int main(int argc, char* argv[]){
  // Nested vector representing lines of Tokens
  // Needs to be used here to cleanup in the case
  // of an exception
  vector< vector<Token*> > tokLines;
  
  // Vector representing tokens in a line
  // Needs to be used here to cleanup in the case
  // of an exception
  vector<Token *> tokLine;

  // Token to use when checking whether address value
  // of a label is within range
  //Token* addressValToken = 0;

  try{
    // Create a MIPS recognizer to tokenize the input line
    Lexer lexer;
    // Tokenize each line of the input
    string line;
    // Line counter
    unsigned int lineCount = 0;
    // Symbol table to store labels with their associated address values
    map<string, int> symbolTable;
    // Vector representing addresses to be relocated
    vector<int> relocAdds;

    while(getline(cin,line)) {
      //if (addressValToken != 0) {                                   // addressValToken isn't NULL?
      //   delete addressValToken;
      //} // if

      tokLine = lexer.scan(line);                                   // Stores tokens that have been made from line read in

      if (tokLine.size() == 0) {                                    // Empty line?
         continue;
      } else {
         // Parse as many tokens in tokLine as possible which are valid labels
         while (tokLine.size() != 0) {
            
            if (tokLine.at(0)->getKind() == ASM::LABEL) {           // Token in line is a label?
               string label = tokLine.at(0)->getLexeme();           // Stores actual string representing token
               label.erase(label.end()-1);                          // Removes semicolon from actual string representing token
               
               if (!symbolTable.count(label)) {                     // Label not in symbol table?
                  //ostringstream oss;
                  //oss << lineCount * 4;
                  //addressValToken = Token::makeToken(ASM::INT, oss.str()); // Need to delete

                  //if (addressValToken->toInt()) {                   // Address value within range?
                     symbolTable[label] = lineCount * 4;            // Adds label and associated address values to symbol table
                     delete tokLine.at(0);                          // Deletes token parsed as valid label
                     tokLine.erase(tokLine.begin());                // Removes label from tokLine
                     continue;
                  //} // if
               } else {
                  throw string("ERROR: Duplicate symbol " + label);
               } // if
            
            } else {
               break;
            } // if
         
         } // while
         
         if (tokLine.size() == 0) {                                 // Empty line?
            continue;
         } else {

            if (tokLine.at(0)->getKind() == ASM::DOTWORD) {         // Token in line is .word?
               
               if (tokLine.size() == 1) {                           // No operand?
                  throw string("ERROR: Needed an integer after directive .word");
               } else if (tokLine.size() > 2) {                     // More operands than required?
                  throw string("ERROR: Expecting end of line, but there's more stuff");
               } else {
                  
                  if (tokLine.at(1)->getKind() == ASM::INT ||
                     tokLine.at(1)->getKind() == ASM::HEXINT) {     // Operand is integer or hexidecimal integer?
                     
                     if (tokLine.at(1)->toInt() == 0 ||
                        tokLine.at(1)->toInt()) {                   // Operand within range?
                        tokLines.push_back(tokLine);
                        tokLine.clear();
                        ++lineCount;
                     } // if
                  
                  } else if (tokLine.at(1)->getKind() == ASM::ID) { // Operand is identifier?
                     
                     tokLines.push_back(tokLine);
                     tokLine.clear();
                     const int relocAdd = ((lineCount + 3) * 4);
                     relocAdds.push_back(relocAdd);
                     ++lineCount;

                  } else {
                     throw string("ERROR: Needed an integer after directive .word");
                  } // if
               
               } // if
            
            } else if (tokLine.at(0)->getKind() == ASM::ID) {                                         // Token in line is identifier?
               
               if (tokLine.at(0)->getLexeme() == "jr" || tokLine.at(0)->getLexeme() == "jalr" ||
                  tokLine.at(0)->getLexeme() == "lis" || tokLine.at(0)->getLexeme() == "mflo" ||
                  tokLine.at(0)->getLexeme() == "mfhi") {                                             // jr or jalr or lis or mflo or mfhi opcode?
                  
                  if (tokLine.size() == 1) {                                                          // No operand?
                     throw string("ERROR: Expecting register, but got end of line");
                  } else if (tokLine.size() > 2) {                                                    // More operands than required?
                     throw string("ERROR: Expecting end of line, but there's more stuff");
                  } else {

                     if (tokLine.at(1)->getKind() == ASM::REGISTER) {                                 // Operand is register?
                      
                        if (tokLine.at(1)->toInt() == 0 ||
                           tokLine.at(1)->toInt()) {                                                  // Operand within range?
                           tokLines.push_back(tokLine);
                           tokLine.clear();
                           ++lineCount;
                        } // if
                     
                     } else {
                        throw string("ERROR: Expecting register, but got " +
                         tokLine.at(1)->toString() + " {" + tokLine.at(1)->getLexeme() + "}");
                     } // if

                  } // if
               
               } else if (tokLine.at(0)->getLexeme() == "add" ||
                  tokLine.at(0)->getLexeme() == "sub" || tokLine.at(0)->getLexeme() == "slt" ||
                  tokLine.at(0)->getLexeme() == "sltu") {                                            // add or sub or slt or sltu opcode?

                  if (tokLine.size() == 1) {                                                         // No operand?
                     throw string("ERROR: Expecting register, but got end of line");
                  } else if (tokLine.size() > 6){                                                    // More commas and operands required?
                     throw string("ERROR: Expecting end of line, but there's more stuff");
                  } else {

                     if (tokLine.at(1)->getKind() == ASM::REGISTER) {                                // Destination register present?
                        
                        if (tokLine.at(1)->toInt() == 0 ||
                           tokLine.at(1)->toInt()) {                                                 // Destination register within range?
                           if (tokLine.size() > 2) {                                                 // End of line not encountered?
                              if (tokLine.at(2)->getKind() == ASM::COMMA) {                          // First comma present?
                                 if (tokLine.size() > 3) {                                           // End of line not encountered?
                                    if (tokLine.at(3)->getKind() == ASM::REGISTER) {                 // First operand register present?
                                       if (tokLine.at(3)->toInt() == 0 ||
                                          tokLine.at(3)->toInt()) {                                  // First operand register within range?
                                          if (tokLine.size() > 4) {                                  // End of line not encountered?
                                              if (tokLine.at(4)->getKind() == ASM::COMMA) {          // Second comma present?
                                                 if (tokLine.size() > 5) {                           // End of line not encountered?
                                                    if (tokLine.at(5)->getKind() == ASM::REGISTER) { // Second operand register present?
                                                       if (tokLine.at(5)->toInt() == 0 ||
                                                          tokLine.at(5)->toInt()) {                  // Second operand register within range?
                                                          tokLines.push_back(tokLine);
                                                          tokLine.clear();
                                                          ++lineCount;
                                                       } // if
                                                    } else {
                                                       throw string("ERROR: Expecting register, but got " +
                                                       tokLine.at(5)->toString() + " {" + tokLine.at(5)->getLexeme() + "}");
                                                    } // if
                                                 } else {
                                                    throw string("ERROR: Expecting register, but got end of line");
                                                 } // if
                                              } else {
                                                 throw string("ERROR: Expecting comma, but got " +
                                                 tokLine.at(4)->toString() + " {" + tokLine.at(4)->getLexeme() + "}");
                                              } // if
                                           } else {
                                              throw string("ERROR: Expecting comma, but got end of line");
                                           } // if
                                       } // if
                                    } else {
                                       throw string("ERROR: Expecting register, but got " +
                                       tokLine.at(3)->toString() + " {" + tokLine.at(3)->getLexeme() + "}");
                                    } // if
                                 } else {
                                    throw string("ERROR: Expecting register, but got end of line");
                                 } // if
                              } else {
                                 throw string("ERROR: Expecting comma, but got " +
                                 tokLine.at(2)->toString() + " {" + tokLine.at(2)->getLexeme() + "}");
                              } // if
                           } else {
                              throw string("ERROR: Expecting comma, but got end of line");
                           } // if
                        } // if

                     } else {
                        throw string("ERROR: Expecting register, but got " +
                         tokLine.at(1)->toString() + " {" + tokLine.at(1)->getLexeme() + "}");
                     } // if

                  } // if

               } else if (tokLine.at(0)->getLexeme() == "bne" || tokLine.at(0)->getLexeme() == "beq") {  // bne or beq opcode?
             
                  if (tokLine.size() == 1) {                                                             // No operand?
                     throw string("ERROR: Expecting register, but got end of line");
                  } else if (tokLine.size() > 6) {                                                       // More commas and operand than required?
                     throw string("ERROR: Expecting end of line, but there's more stuff");
                  } else {

                     if (tokLine.at(1)->getKind() == ASM::REGISTER) {                                    // First comparand register present?
                        if (tokLine.at(1)->toInt() == 0 || tokLine.at(1)->toInt()) {                     // First comparand register within range?
                           if (tokLine.size() > 2) {                                                     // End of line not encountered?
                              if (tokLine.at(2)->getKind() == ASM::COMMA) {                              // First comma present?
                                 if (tokLine.size() > 3) {                                               // End of line not encountered?
                                    if (tokLine.at(3)->getKind() == ASM::REGISTER) {                     // Second comparand register present?
                                       if (tokLine.at(3)->toInt() == 0 || tokLine.at(3)->toInt()) {      // Second comparand register within range?
                                          if (tokLine.size() > 4) {                                      // End of line not encountered?
                                             if (tokLine.at(4)->getKind() == ASM::COMMA) {               // Second comma present?
                                                if (tokLine.size() > 5) {                                // End of line not encountered?
                                                   if (tokLine.at(5)->getKind() == ASM::INT) {           // Immediate value is integer?
                                                      if (tokLine.at(5)->toInt() < -32768 ||
                                                         tokLine.at(5)->toInt() > 32767) {            // Immediate value isn't within range?
                                                         throw string("ERROR: Branch offset out of range");
                                                      } else {
                                                         tokLines.push_back(tokLine);
                                                         tokLine.clear();
                                                         ++lineCount;
                                                      } // if
                                                   } else if (tokLine.at(5)->getKind() == ASM::HEXINT) { // Immediate value is hexadecimal integer? 
                                                      if (tokLine.at(5)->toInt() > 0xffff ||
                                                         tokLine.at(5)->toInt() < 0) {                // Immediate value isn't within range?
                                                         throw string("ERROR: Branch offset out of range");
                                                      } else {
                                                         tokLines.push_back(tokLine);
                                                         tokLine.clear();
                                                         ++lineCount;
                                                      } // if
                                                   } else if (tokLine.at(5)->getKind() == ASM::ID) {     // Token in line is identifier?
                                                      tokLines.push_back(tokLine);
                                                      tokLine.clear();
                                                      ++lineCount;
                                                   } else {
                                                      throw string("ERROR: Expecting immediate, but got " +
                                                      tokLine.at(5)->toString() + " {" + tokLine.at(5)->getLexeme() + "}");
                                                   } // if
                                                } else {
                                                   throw string("ERROR: Expecting immediate, but got end of line");
                                                } // if
                                             } else {
                                                throw string("ERROR: Expecting comma, but got " +
                                                tokLine.at(4)->toString() + " {" + tokLine.at(4)->getLexeme() + "}");
                                             } // if
                                          } else {
                                             throw string("ERROR: Expecting comma, but got end of line");
                                          } // if
                                       } // if
                                    } else {
                                       throw string("ERROR: Expecting register, but got " +
                                       tokLine.at(3)->toString() + " {" + tokLine.at(3)->getLexeme() + "}");
                                    } // if
                                 } else {
                                    throw string("ERROR: Expecting register, but got end of line");
                                 } // if
                              } else {
                                 throw string("ERROR: Expecting comma, but got " +
                                 tokLine.at(2)->toString() + " {" + tokLine.at(2)->getLexeme() + "}");
                              } // if
                           } else {
                              throw string("ERROR: Expecting comma, but got end of line");
                           } // if
                        } // if
                     } else {
                        throw string("ERROR: Expecting register, but got " +
                         tokLine.at(1)->toString() + " {" + tokLine.at(1)->getLexeme() + "}");
                     } // if

                  } // if

               } else if (tokLine.at(0)->getLexeme() == "mult" || tokLine.at(0)->getLexeme() == "multu" ||
                  tokLine.at(0)->getLexeme() == "div" || tokLine.at(0)->getLexeme() == "divu") {            // mult or multu or div or divu opcode? 

                  if (tokLine.size() == 1) {                                                                // No operand?
                     throw string("ERROR: Expecting register, but got end of line");
                  } else if (tokLine.size() > 4) {                                                          // More commas and operands than required?
                     throw string("ERROR: Expecting end of line, but there's more stuff");
                  } else {

                     if (tokLine.at(1)->getKind() == ASM::REGISTER) {                                       // First operand register present?
                        if (tokLine.at(1)->toInt() == 0 || tokLine.at(1)->toInt()) {                        // First operand register within range?
                           if (tokLine.size() > 2) {                                                        // End of line not encountered?
                              if (tokLine.at(2)->getKind() == ASM::COMMA) {                                 // Comma present?
                                 if (tokLine.size() > 3) {                                                  // End of line not encountered?
                                    if (tokLine.at(3)->getKind() == ASM::REGISTER) {                        // Second operand register present?
                                       if (tokLine.at(3)->toInt() == 0 || tokLine.at(3)->toInt()) {         // Second operand register within range?
                                          tokLines.push_back(tokLine);
                                          tokLine.clear();
                                          ++lineCount;
                                       } // if
                                    } else {
                                       throw string("ERROR: Expecting register, but got " +
                                       tokLine.at(3)->toString() + " {" + tokLine.at(3)->getLexeme() + "}");
                                    } // if
                                 } else {
                                    throw string("ERROR: Expecting register, but got end of line");
                                 } // if
                              } else {
                                 throw string("ERROR: Expecting comma, but got " +
                                 tokLine.at(2)->toString() + " {" + tokLine.at(2)->getLexeme() + "}");
                              } // if
                           } else {
                              throw string("ERROR: Expecting comma, but got end of line");
                           } // if
                        } // if
                     } else {
                        throw string("ERROR: Expecting register, but got " +
                         tokLine.at(1)->toString() + " {" + tokLine.at(1)->getLexeme() + "}");
                     } // if

                  } // if

               } else if (tokLine.at(0)->getLexeme() == "lw" || tokLine.at(0)->getLexeme() == "sw") {      // lw or sw opcode?
                  
                  if (tokLine.size() == 1) {                                                               // No operand?
                     throw string("ERROR: Expecting register, but got end of line");
                  } else if (tokLine.size() > 7) {
                     throw string("ERROR: Expecting end of line, but there's more stuff");
                  } else {

                     if (tokLine.at(1)->getKind() == ASM::REGISTER) {                                      // First operand register present?
                        if (tokLine.at(1)->toInt() == 0 || tokLine.at(1)->toInt()) {                       // First operand register within range?
                           if (tokLine.size() > 2) {                                                       // End of line not encountered?
                              if (tokLine.at(2)->getKind() == ASM::COMMA) {                                // Comma present?
                                 if (tokLine.size() > 3) {                                                 // End of line not encountered?
                                    if (tokLine.at(3)->getKind() == ASM::INT) {                            // Immediate value is integer?
                                       if (tokLine.at(3)->toInt() >= -32768 &&
                                          tokLine.at(3)->toInt() <= 32767) {                               // Immediate value is within range?
                                           if (tokLine.size() > 4) {                                       // End of line not encountered?
                                              if (tokLine.at(4)->getKind() == ASM::LPAREN) {               // Left parentheses present?
                                                 if (tokLine.size() > 5) {                                 // End of line not encountered?
                                                    if (tokLine.at(5)->getKind() == ASM::REGISTER) {       // Second operand register present?
                                                       if (tokLine.at(5)->toInt() == 0 ||
                                                          tokLine.at(5)->toInt()) {                        // Second operand register withi range?
                                                          if (tokLine.size() > 6) {                        // End of line not encountered?
                                                             if (tokLine.at(6)->getKind() == ASM::RPAREN) {// Right parentheses present?
                                                                tokLines.push_back(tokLine);
                                                                tokLine.clear();
                                                                ++lineCount;
                                                             } else {
                                                                throw string("ERROR: Expecting right parentheses, but got " +
                                                                tokLine.at(6)->toString() + " {" + tokLine.at(6)->getLexeme() + "}");
                                                             } // if
                                                          } else {
                                                             throw string("ERROR: Expecting right parentheses, but got end of line");
                                                          } // if
                                                       } // if
                                                    } else {
                                                       throw string("ERROR: Expecting register, but got " +
                                                       tokLine.at(5)->toString() + " {" + tokLine.at(5)->getLexeme() + "}");
                                                    } // if
                                                 } else {
                                                    throw string("ERROR: Expecting register, but got end of line");
                                                 } // if
                                              } else {
                                                 throw string("ERROR: Expecting immediate, but got " +
                                                 tokLine.at(4)->toString() + " {" + tokLine.at(4)->getLexeme() + "}");
                                              } // if
                                           } else {
                                              throw string("ERROR: Expecting left parentheses, but got end of line");
                                           } // if
                                       } else {
                                          throw string("ERROR: Branch offset out of range");
                                       } // if
                                    } else if (tokLine.at(3)->getKind() == ASM::HEXINT) {                  // Immediate value is hexadecimal integer?
                                       if (tokLine.at(3)->toInt() <= 0xffff &&
                                          tokLine.at(3)->toInt() >= 0) {                                   // Immediate value is within range?
                                          if (tokLine.size() > 4) {                                       // End of line not encountered?
                                              if (tokLine.at(4)->getKind() == ASM::LPAREN) {               // Left parentheses present?
                                                 if (tokLine.size() > 5) {                                 // End of line not encountered?
                                                    if (tokLine.at(5)->getKind() == ASM::REGISTER) {       // Second operand register present?
                                                       if (tokLine.at(5)->toInt() == 0 ||
                                                          tokLine.at(5)->toInt()) {                        // Second operand register withi range?
                                                          if (tokLine.size() > 6) {                        // End of line not encountered?
                                                             if (tokLine.at(6)->getKind() == ASM::RPAREN) {// Right parentheses present?
                                                                tokLines.push_back(tokLine);
                                                                tokLine.clear();
                                                                ++lineCount;
                                                             } else {
                                                                throw string("ERROR: Expecting right parentheses, but got " +
                                                                tokLine.at(6)->toString() + " {" + tokLine.at(6)->getLexeme() + "}");
                                                             } // if
                                                          } else {
                                                             throw string("ERROR: Expecting right parentheses, but got end of line");
                                                          } // if
                                                       } // if
                                                    } else {
                                                       throw string("ERROR: Expecting register, but got " +
                                                       tokLine.at(5)->toString() + " {" + tokLine.at(5)->getLexeme() + "}");
                                                    } // if
                                                 } else {
                                                    throw string("ERROR: Expecting register, but got end of line");
                                                 } // if
                                              } else {
                                                 throw string("ERROR: Expecting immediate, but got " +
                                                 tokLine.at(4)->toString() + " {" + tokLine.at(4)->getLexeme() + "}");
                                              } // if
                                           } else {
                                              throw string("ERROR: Expecting left parentheses, but got end of line");
                                           } // if
                                       } else {
                                          throw string("ERROR: Branch offset out of range");
                                       } // if
                                    } else {
                                       throw string("ERROR: Expecting immediate, but got " +
                                       tokLine.at(3)->toString() + " {" + tokLine.at(3)->getLexeme() + "}");
                                    } // if
                                 } else {
                                    throw string("ERROR: Expecting immediate, but got end of line");
                                 } // if
                              } else {
                                 throw string("ERROR: Expecting comma, but got " +
                                 tokLine.at(2)->toString() + " {" + tokLine.at(2)->getLexeme() + "}");
                              } // if
                           } else {
                              throw string("ERROR: Expecting comma, but got end of line");
                           } // if
                        } // if
                     } else {
                        throw string("ERROR: Expecting register, but got " +
                         tokLine.at(1)->toString() + " {" + tokLine.at(1)->getLexeme() + "}");
                     } // if

                  } // if

               } else {
                  throw string("ERROR: Illegal opcode " + tokLine.at(0)->getLexeme()); 
               } // if
               
            } else {
               throw string("ERROR: Expecting opcode, label, or directive, but got "
                + tokLine.at(0)->toString() + " {" + tokLine.at(0)->getLexeme() + "}");
            } // if

         } // if
      } // if
    } // while

    const int cookie = 0x10000002;                                   // Cookie for MERL file
    const int codeLen = ((lineCount + 3) * 4);                       // Length of code
    const int fileLen = ((relocAdds.size() * 2 * 4) + codeLen);      // Length of MERL file
    
    output_word(cookie);
    output_word(fileLen);
    output_word(codeLen);

    // Iterates over lines of tokens and prints machine instructions to standard output
    for (unsigned int j = 0; j < tokLines.size(); ++j) {
       vector<Token *> curLine;
       curLine = tokLines.at(j);
       if (curLine.at(0)->getKind() == ASM::DOTWORD) {               // Token in line is .word?
          
          if (curLine.at(1)->getKind() == ASM::INT ||
             curLine.at(1)->getKind() == ASM::HEXINT) {              // Operand is integer?
             int i = curLine.at(1)->toInt();                         // Converts operand to integer
             output_word(i);
          } else {
             if (symbolTable.count(curLine.at(1)->getLexeme())) {    // Label is in symbol table?
                int i = symbolTable[curLine.at(1)->getLexeme()];
                output_word(i + (3 * 4));
             } else {
                throw string("ERROR: No such label: " + curLine.at(1)->getLexeme());
             } // if
          } // if

       } else {
          
          if (curLine.at(0)->getLexeme() == "jr") {                  // jr opcode?
             int s = curLine.at(1)->toInt();
             output_jtype(s, 8);
          } else if (curLine.at(0)->getLexeme() == "jalr") {         // jalr opcode?
             int s = curLine.at(1)->toInt();
             output_jtype(s, 9);
          } else if (curLine.at(0)->getLexeme() == "lis") {          // lis opcode?
             int d = curLine.at(1)->toInt();
             output_rtype(0, 0, d, 20);
          } else if (curLine.at(0)->getLexeme() == "mflo") {         // mflo opcode?
             int d = curLine.at(1)->toInt();
             output_rtype(0, 0, d, 18);
          } else if (curLine.at(0)->getLexeme() == "mfhi") {         // mfhi opcode?
             int d = curLine.at(1)->toInt();
             output_rtype(0, 0, d, 16);
          } else if (curLine.at(0)->getLexeme() == "add") {          // add opcode?
             int d = curLine.at(1)->toInt();
             int s = curLine.at(3)->toInt();
             int t = curLine.at(5)->toInt();
             output_rtype(s, t, d, 32);
          } else if (curLine.at(0)->getLexeme() == "sub") {          // sub opcode?
             int d = curLine.at(1)->toInt();
             int s = curLine.at(3)->toInt();
             int t = curLine.at(5)->toInt();
             output_rtype(s, t, d, 34);
          } else if (curLine.at(0)->getLexeme() == "slt") {          // slt opcode?
             int d = curLine.at(1)->toInt();
             int s = curLine.at(3)->toInt();
             int t = curLine.at(5)->toInt();
             output_rtype(s, t, d, 42);
          } else if (curLine.at(0)->getLexeme() == "sltu") {         // sltu opcode?
             int d = curLine.at(1)->toInt();
             int s = curLine.at(3)->toInt();
             int t = curLine.at(5)->toInt();
             output_rtype(s, t, d, 43);
          } else if (curLine.at(0)->getLexeme() == "mult") {         // mult opcode?
             int s = curLine.at(1)->toInt();
             int t = curLine.at(3)->toInt();
             output_rtype(s, t, 0, 24);
          } else if (curLine.at(0)->getLexeme() == "multu") {        // multu opcode?
             int s = curLine.at(1)->toInt();
             int t = curLine.at(3)->toInt();
             output_rtype(s, t, 0, 25);
          } else if (curLine.at(0)->getLexeme() == "div") {          // div opcode?
             int s = curLine.at(1)->toInt();
             int t = curLine.at(3)->toInt();
             output_rtype(s, t, 0, 26);
          } else if (curLine.at(0)->getLexeme() == "divu") {         // divu opcode?
             int s = curLine.at(1)->toInt();
             int t = curLine.at(3)->toInt();
             output_rtype(s, t, 0, 27);
          } else if (curLine.at(0)->getLexeme() == "lw") {           // lw opcode?
             int t = curLine.at(1)->toInt();
             int i = curLine.at(3)->toInt();
             int s = curLine.at(5)->toInt();
             output_itype(35, s, t, i);
          } else if (curLine.at(0)->getLexeme() == "sw") {           // sw opcode?
             int t = curLine.at(1)->toInt();
             int i = curLine.at(3)->toInt();
             int s = curLine.at(5)->toInt();
             output_itype(43 , s, t, i);
          } else if (curLine.at(0)->getLexeme() == "beq") {          // beq opcode?
             int s = curLine.at(1)->toInt();
             int t = curLine.at(3)->toInt();
             
             if (curLine.at(5)->getKind() == ASM::INT) {             // Operand is integer?
                int i = curLine.at(5)->toInt();
                output_itype(4, s, t, i);
             } else if (curLine.at(5)->getKind() == ASM::HEXINT) {   // Operand is hexadecimal integer? 
                int i = curLine.at(5)->toInt();
                output_itype(4, s, t, i);
             } else {
                
                if (symbolTable.count(curLine.at(5)->getLexeme())) { // Label is in symbol table?
                   int i = symbolTable[curLine.at(5)->getLexeme()];
                   const int branchOffset = (i - (j * 4) - 4);
                   const int adjustedBranchOffset = (branchOffset / 4);
                   if (adjustedBranchOffset >= -32768 &&
                      adjustedBranchOffset <= 32767) {               // Immediate value within range?
                      output_itype(4, s, t, ((i - ((j + 1) * 4)) / 4));
                   } else {
                      throw string("ERROR: Branch offset out of range");
                   } // if 
                } else {
                   throw string("ERROR: No such label: " + curLine.at(5)->getLexeme());
                } // if

             } // if

          } else {
             int s = curLine.at(1)->toInt();
             int t = curLine.at(3)->toInt();

             if (curLine.at(5)->getKind() == ASM::INT) {             // Operand is integer?
                int i = curLine.at(5)->toInt();
                output_itype(5, s, t, i);
             } else if (curLine.at(5)->getKind() == ASM::HEXINT) {   // Operand is integer?
                int i = curLine.at(5)->toInt();
                output_itype(5, s, t, i);
             } else {

                if (symbolTable.count(curLine.at(5)->getLexeme())) { // Label is in symbol table?
                   int i = symbolTable[curLine.at(5)->getLexeme()];
                   const int branchOffset = (i - (j * 4) - 4);
                   const int adjustedBranchOffset = (branchOffset / 4);
                   if (adjustedBranchOffset >= -32768 &&
                      adjustedBranchOffset <= 32767) {               // Immediate value within range?
                      output_itype(5, s, t, ((i - ((j + 1) * 4)) / 4));
                   } else {
                      throw string("ERROR: Branch offset out of range");
                   } // if
                } else {
                   throw string("ERROR: No such label: " + curLine.at(5)->getLexeme());
                } // if

             } // if

          } // if

       } // if
    
    } // for

    // Iterates over vector of addresses to be relocated and prints them out to
    // standard output
    for (vector<int>::iterator it = relocAdds.begin(); it != relocAdds.end(); ++it) {
       const int formatCode = 0x00000001;                        // Format code for relocation entry
       output_word(formatCode);
       output_word(*it);
    } // for

  } catch (const string& msg) {
    // If an exception occurs print the message and end the program
    cerr << msg << endl;
  }

  //if (addressValToken != 0) {                                  // addressValToken isn't NULL?
  //   delete addressValToken;
  //} // if

  // Delete tokens that have been made in tokLine
  for (vector <Token *>::iterator it = tokLine.begin(); it != tokLine.end(); ++it) {
     delete *it;
  } // for
  
  // Delete tokens that have been made in tokLines
  for(vector< vector<Token *> >::iterator it = tokLines.begin(); it != tokLines.end(); ++it){
    for(vector<Token *>::iterator it2 = it->begin(); it2 != it->end(); ++it2){
      delete *it2;
    } // for
  } // for
} // main
