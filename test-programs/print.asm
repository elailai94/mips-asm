;; Program to print out the digits entered to the screen.

; Registers needed:
; $1 = 32-bit integer
; $4 = size of a word
; $5 = value of 1
; $6 = value of 10
; $7 = value of 45
; $8 = value of 48
; $9 = address of standard output
; $10 = loop counter
; $11 = number of divisions performed counter
; $12 = rough work
; $13 = rough work
 
; Initializes $4 to $11, and $14
lis $4
.word 4
lis $5
.word 1
lis $6
.word 10
lis $7
.word 45
lis $8
.word 48
lis $9
.word 0xFFFF000C
add $10, $0, $1
add $11, $0, $0
lis $14
.word -2147483648

; Bounds correction
bne $1, $14, absolute
add $10, $10, $5

; Takes absolute value of $10
absolute:
slt $12, $10, $0
beq $12, $0, top1
sub $12, $0, $5
mult $10, $12
mflo $10
 
; Iterates until loop counter reaches 0 
top1:
div $10, $6
mflo $12
beq $12, $0, printSign

;; Updates loop & number of divisions performed counters
add $10, $0, $12
add $11, $11, $5

;; Push remainder on stack
mfhi $12
sub $30, $30, $4
sw $12, 0($30)
 
bottom1:
beq $0, $0, top1
 
printSign:
mfhi $12
slt $13, $1, $0
beq $13, $0, printFirstDigit
sw $7, 0($9)

printFirstDigit:
add $12, $8, $12
sw $12, 0($9)
 
; Iterates until number of divisions counter reaches 0
top2:
beq $11, $0, printNewline
; Pop remainder off stack
lw $12, 0($30)
bne $11, $5, 2
bne $1, $14, 1
add $12, $5, $12
add $30, $30, $4
 
;; printRemainingDigits:
add $12, $8, $12
sw $12, 0($9)
 
;; Decrements number of divisions counter
sub $11, $11, $5
 
bottom2:
beq $0, $0, top2

printNewline:
sw $6, 0($9)
 
end:
jr $31
